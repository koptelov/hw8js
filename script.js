/**1
 * Опишіть своїми словами що таке Document Object Model (DOM)
 *
 * DOM з'єднує HTML сторінку з язіками програмування які
 * будуть робити якісь дії на веб сторінці. наприклад
 * робити її більш інтерективною.
 *
 *
 *
 * 2
 * Яка різниця між властивостями HTML-елементів
 *  innerHTML та innerText?
 *
 * innerText показує тектсовий вміст який між відкривающими
 * та закривающими тегами
 *
 * innerHTML  показує текстову інформацію тільки по одному
 * елементу. у вивід попаде і текст і розмітка HTML документа
 * яка може бути між відкривающими та закривающими основними
 * тегами.
 *
 *
 *
 * 3.
 * Як можна звернутися до елемента сторінки
 * за допомогою JS? Який спосіб кращий?
 *
 *  прописати document. із нього можно звернутися до
 *  любого єлемента потім вже documentElement це весь HTML
 * документ, потім document.body   та далі тому подібне.
 *

 */
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.getElementsByTagName("p");
for (var i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = "#ff0000";
}

// Знайти елемент із id="optionsList". Вивести у консоль.
//  Знайти батьківський елемент та вивести в консоль.
//  Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentNode);
let childNodes = optionsList.childNodes;
for (let i = 0; i < childNodes.length; i++) {
  console.log(childNodes[i].nodeName + " " + childNodes[i].nodeType);
}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

let testParagraph = document.getElementById("testParagraph");
testParagraph.innerHTML = "This is a paragraph";

/**
  Отримати елементи    вкладені в елемент 
 із класом main-header і вивести їх у консоль.
 Кожному з елементів присвоїти новий клас nav-item.
 */

let mainHeader = document.getElementsByClassName("main-header")[0];
let navItems = mainHeader.getElementsByTagName("li");
for (let i = 0; i < navItems.length; i++) {
  navItems[i].className = "nav-item";
  console.log(navItems[i]);
}

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitles = document.getElementsByClassName("section-title");
for (let i = 0; i < sectionTitles.length; i++) {
  sectionTitles[i].className = "";
}
